import Foundation

class RepositoryListDefaultInteractor: RepositoryListInteractorInput {

    weak var delegate: RepositoryListInteractorOutput?

    fileprivate let requestManager: AnyRequestManager

    fileprivate let group = DispatchGroup()

    fileprivate let repositoryQueue = DispatchQueue(label: "\(RepositoryListDefaultInteractor.self).RepositoryQueue")

    fileprivate let firstHalfSearchQueue = DispatchQueue(label: "\(RepositoryListDefaultInteractor.self)Queue1",
                                                         attributes: .concurrent)

    fileprivate let secondHalfSearchQueue = DispatchQueue(label: "\(RepositoryListDefaultInteractor.self)Queue2",
                                                          attributes: .concurrent)

    fileprivate var repositories: [Repository] = []

    fileprivate var currentSearchKeyword: String?

    init() {
        requestManager = AlamofireRequestManager(configuration: NetworkServiceConfiguration())
    }

    func fetchDatabaseRepositories() {
        repositoryQueue.async { [weak self] in
            let repositoryRepo: RepositoryRepository = RealmRepositoryRepository()
            let repositories = repositoryRepo.repositories

            self?.delegate?.didFinishFetchingDatabaseRepositories(repositories)
        }
    }

    func searchRepositories(by keyword: String) {
        cancelSearch()
        currentSearchKeyword = keyword

        let request = SearchRequest(for: keyword, sortedBy: .stars, orderedBy: .descending)

        performSearchRequest(request, onQueue: firstHalfSearchQueue)
        performSearchRequest(request, onQueue: secondHalfSearchQueue)

        group.notify(queue: repositoryQueue) { [weak self] in
            guard let welf = self else { return } // `welf` is abbreviation from `weak self`
            guard welf.currentSearchKeyword != nil else { return }

            welf.delegate?.didFinishSearchingRepositories(welf.repositories)
        }

        delegate?.didStartSearchingRepositories()
    }

    func cancelSearch() {
        repositories.removeAll()
        currentSearchKeyword = nil

        delegate?.didCancelSearchingRepositories()
    }

    func saveRepositories(_ repositories: [Repository]) {
        repositoryQueue.async {
            let repositoryRepo: RepositoryRepository = RealmRepositoryRepository()
            repositoryRepo.insert(repositories, orUpdateExisting: true)
        }
    }
}

// MARK: - Private methods
private extension RepositoryListDefaultInteractor {

    func performSearchRequest(_ request: SearchRequest, onQueue queue: DispatchQueue) {
        queue.async { [weak self] in
            let searchCall = RequestCall(request: request, responseType: SearchResponse.self) {
                guard let welf = self else { return }

                switch $0 {
                case .success(let response):
                    if welf.currentSearchKeyword != nil && welf.currentSearchKeyword == response.value.keyword {
                        if welf.repositories.isEmpty {
                            welf.repositories += response.value.repositories.leftHalf
                        } else {
                            welf.repositories += response.value.repositories.rightHalf
                        }
                    }

                case .failure(let error):
                    welf.delegate?.didFailToSearchRepositories(withError: error)
                }

                welf.group.leave()
            }
            self?.requestManager.request(searchCall)
        }

        group.enter()
    }
}
