import UIKit

class RepositoryListDefaultWireframe: RepositoryListWireframeInput {

    func presentRepositoryList(from window: UIWindow, with wireframe: RootWireframe) {
        let builder: RepositoryListModuleBuilder = RepositoryListModuleDefaultBuilder()
        let view = builder.build()

        wireframe.presentRootViewController(view, in: window)
    }

    func presentViewRepository(from view: UIViewController,
                               configuration: ViewRepositoryModuleConfiguration,
                               delegate: ViewRepositoryModuleDelegate?) {
        let builder: ViewRepositoryModuleBuilder = ViewRepositoryModuleDefaultBuilder()
        let viewRepositoryViewController = builder.build(with: configuration, delegate: delegate)
        viewRepositoryViewController.modalPresentationStyle = .custom

        view.present(viewRepositoryViewController, animated: true)
    }
}
