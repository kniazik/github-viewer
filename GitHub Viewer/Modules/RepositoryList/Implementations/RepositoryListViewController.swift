import UIKit

class RepositoryListViewController: UIViewController {

    fileprivate struct Constant {
        static let cellIdentifier = "RepositoryCell"
    }

    @IBOutlet fileprivate weak var tvRepositories: UITableView!

    @IBOutlet fileprivate weak var sbRepositories: UISearchBar!

    @IBOutlet fileprivate weak var bCancelSearch: UIBarButtonItem!

    var delegate: RepositoryListViewOutput?

    override func viewDidLoad() {
        super.viewDidLoad()

        tvRepositories.register(UINib(nibName: Constant.cellIdentifier, bundle: Bundle.main),
                                forCellReuseIdentifier: Constant.cellIdentifier)
        delegate?.didLoad()
    }
}

// MARK: - Actions
private extension RepositoryListViewController {

    @IBAction func historyAction(_ sender: UIBarButtonItem) {
        delegate?.didTapHistoryButton()
    }

    @IBAction func cancelSearchAction(_ sender: UIBarButtonItem) {
        delegate?.didTapCancelButton()
    }
}

// MARK: - RepositoryListViewInput
extension RepositoryListViewController: RepositoryListViewInput {

    func updateView() {
        DispatchQueue.main.async { [weak self] in
            self?.tvRepositories.reloadData()
        }
    }

    func setCancelButton(enabled: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.bCancelSearch.isEnabled = enabled
        }
    }

    func showError(message: String) {
        let alertController = UIAlertController(title: "Error",
                                                message: message,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(okAction)

        present(alertController, animated: true)
    }

    func dismissPresentedView() {
        presentedViewController?.dismiss(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension RepositoryListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.numberOfRepositories(in: section) ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.cellIdentifier) as? RepositoryCell else {
            return RepositoryCell()
        }

        guard let dataSource = delegate?.dataSource(forRow: indexPath.row, inSection: indexPath.section) else {
            return cell
        }

        cell.configure(with: dataSource)

        return cell
    }
}

// MARK: - UITableViewDelegate
extension RepositoryListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        view.endEditing(true)

        delegate?.didSelect(row: indexPath.row, inSection: indexPath.section)
    }
}

// MARK: - UISearchBarDelegate
extension RepositoryListViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        delegate?.didTapSearchButton(with: searchBar.text)
    }
}
