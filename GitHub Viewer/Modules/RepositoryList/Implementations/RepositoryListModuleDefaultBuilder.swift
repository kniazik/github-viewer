import UIKit

class RepositoryListModuleDefaultBuilder: RepositoryListModuleBuilder {

    private struct Constant {
        static let viewIdentifier = "RepositoryListViewController"
    }

    private var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

    func build() -> UIViewController {
        let view = storyboard.instantiateViewController(withIdentifier: Constant.viewIdentifier) as! RepositoryListViewInput
        let wireframe: RepositoryListWireframeInput = RepositoryListDefaultWireframe()
        let interactor: RepositoryListInteractorInput = RepositoryListDefaultInteractor()
        let presenter: RepositoryListPresenter
        presenter = RepositoryListDefaultPresenter(view: view, wireframe: wireframe, interactor: interactor)
        view.delegate = presenter
        interactor.delegate = presenter

        return view as! UIViewController
    }
}
