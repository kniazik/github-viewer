import UIKit

class RepositoryListDefaultPresenter: RepositoryListPresenter {

    fileprivate struct Constant {

        static let maxCharactersCount = 30
    }

    fileprivate weak var view: RepositoryListViewInput?

    fileprivate let wireframe: RepositoryListWireframeInput

    fileprivate let interactor: RepositoryListInteractorInput

    fileprivate var viewModels: [RepositoryViewModel] = []

    init(view: RepositoryListViewInput,
         wireframe: RepositoryListWireframeInput,
         interactor: RepositoryListInteractorInput) {
        self.view = view
        self.wireframe = wireframe
        self.interactor = interactor
    }
}

// MARK: - RepositoryListViewOutput
extension RepositoryListDefaultPresenter: RepositoryListViewOutput {

    func didLoad() {
        interactor.fetchDatabaseRepositories()
    }

    func numberOfRepositories(in section: Int) -> Int {
        return viewModels.count
    }

    func dataSource(forRow row: Int, inSection section: Int) -> RepositoryCellDataSource? {
        guard row < viewModels.count else { return nil }
        return viewModels[row]
    }

    func didSelect(row: Int, inSection section: Int) {
        guard row < viewModels.count else { return }
        guard let view = view as? UIViewController else { return }

        let configuration = ViewRepositoryModuleConfiguration(title: viewModels[row].name,
                                                              url: viewModels[row].url)
        wireframe.presentViewRepository(from: view, configuration: configuration, delegate: self)
    }

    func didTapSearchButton(with text: String?) {
        guard let text = text, !text.isEmpty else {
            view?.setCancelButton(enabled: false)
            return
        }
        view?.setCancelButton(enabled: true)
        interactor.searchRepositories(by: text)
    }

    func didTapCancelButton() {
        view?.setCancelButton(enabled: false)
        interactor.cancelSearch()
    }

    func didTapHistoryButton() {
        interactor.fetchDatabaseRepositories()
    }
}

// MARK: - RepositoryListInteractorOutput
extension RepositoryListDefaultPresenter: RepositoryListInteractorOutput {

    func didFinishSearchingRepositories(_ repositories: [Repository]) {
        interactor.saveRepositories(repositories)

        let sortedRepositories = repositories.sorted(by: { $0.stars > $1.stars })
        createViewModels(from: sortedRepositories)

        view?.setCancelButton(enabled: false)
        view?.updateView()
    }

    func didFailToSearchRepositories(withError error: Error) {
        view?.showError(message: error.localizedDescription)
    }

    func didFinishFetchingDatabaseRepositories(_ repositories: [Repository]) {
        interactor.cancelSearch()

        let sortedRepositories = repositories.sorted(by: { $0.name < $1.name })
        createViewModels(from: sortedRepositories)

        view?.setCancelButton(enabled: false)
        view?.updateView()
    }
}

// MARK: - ViewRepositoryModuleDelegate
extension RepositoryListDefaultPresenter: ViewRepositoryModuleDelegate {

    func dismissModule() {
        view?.dismissPresentedView()
    }
}

// MARK: - Private methods
private extension RepositoryListDefaultPresenter {

    func createViewModels(from repositories: [Repository]) {
        viewModels = repositories.map {
            RepositoryDefaultViewModel(id: $0.id,
                                       name: $0.name.count > Constant.maxCharactersCount
                                        ? String($0.name[..<Constant.maxCharactersCount])
                                        : $0.name,
                                       description: $0.specification.count > Constant.maxCharactersCount
                                        ? String($0.specification[..<Constant.maxCharactersCount])
                                        : $0.specification,
                                       url: $0.url,
                                       starsCount: $0.stars,
                                       isPrivate: $0.isPrivate,
                                       ownerName: $0.owner.username)
        }
    }
}
