import Foundation

struct RepositoryDefaultViewModel: RepositoryViewModel {

    let id: Int64

    let name: String

    let description: String

    let url: URL

    let starsCount: Int

    let isPrivate: Bool

    let ownerName: String
}
