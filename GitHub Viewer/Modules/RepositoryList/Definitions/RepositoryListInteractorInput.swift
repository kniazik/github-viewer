protocol RepositoryListInteractorInput: class {

    var delegate: RepositoryListInteractorOutput? { get set }

    func fetchDatabaseRepositories()

    func searchRepositories(by keyword: String)

    func cancelSearch()

    func saveRepositories(_ repositories: [Repository])
}
