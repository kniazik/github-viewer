import UIKit

protocol RepositoryListModuleBuilder {

    func build() -> UIViewController
}
