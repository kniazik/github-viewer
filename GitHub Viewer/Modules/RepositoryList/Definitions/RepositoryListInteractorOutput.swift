protocol RepositoryListInteractorOutput: class {

    func didStartSearchingRepositories()

    func didFinishSearchingRepositories(_ repositories: [Repository])

    func didCancelSearchingRepositories()

    func didFailToSearchRepositories(withError error: Error)

    func didFinishFetchingDatabaseRepositories(_ repositories: [Repository])
}

// MARK: - Default implementation
extension RepositoryListInteractorOutput {

    func didStartSearchingRepositories() {}

    func didCancelSearchingRepositories() {}
}
