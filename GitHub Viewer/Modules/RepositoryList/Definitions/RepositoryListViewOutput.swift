import UIKit

protocol RepositoryListViewOutput {

    func didLoad()

    func numberOfRepositories(in section: Int) -> Int

    func dataSource(forRow row: Int, inSection section: Int) -> RepositoryCellDataSource?

    func didSelect(row: Int, inSection section: Int)

    func didTapSearchButton(with text: String?)

    func didTapCancelButton()

    func didTapHistoryButton()
}
