import UIKit

protocol RepositoryListWireframeInput {

    func presentRepositoryList(from window: UIWindow, with wireframe: RootWireframe)

    func presentViewRepository(from view: UIViewController,
                               configuration: ViewRepositoryModuleConfiguration,
                               delegate: ViewRepositoryModuleDelegate?)
}
