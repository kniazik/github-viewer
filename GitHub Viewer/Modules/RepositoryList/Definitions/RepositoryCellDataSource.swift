protocol RepositoryCellDataSource {

    var name: String { get }

    var description: String { get }

    var isPrivate: Bool { get }

    var starsCount: Int { get }
}
