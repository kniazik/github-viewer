import Foundation

protocol RepositoryViewModel: RepositoryCellDataSource {

    var id: Int64 { get }

    var url: URL { get }

    var ownerName: String { get }
}
