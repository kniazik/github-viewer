protocol RepositoryListViewInput: class {

    var delegate: RepositoryListViewOutput? { get set }

    func updateView()

    func setCancelButton(enabled: Bool)

    func showError(message: String)

    func dismissPresentedView()
}
