import UIKit

class RepositoryCell: UITableViewCell {

    fileprivate enum Constant {

        static let privateRepositoryColor = UIColor.red

        static let publicRepositoryColor = UIColor.green
    }

    @IBOutlet fileprivate weak var lName: UILabel!

    @IBOutlet fileprivate weak var lDescription: UILabel!

    @IBOutlet fileprivate weak var lStarsCount: UILabel!

    @IBOutlet fileprivate weak var vPrivateIndicator: UIView!
}

extension RepositoryCell: Configurable {

    func configure(with dataSource: RepositoryCellDataSource) {
        lName.text = dataSource.name
        lDescription.text = dataSource.description
        lStarsCount.text = "Stars: \(dataSource.starsCount)"
        vPrivateIndicator.backgroundColor = dataSource.isPrivate
                                            ? Constant.privateRepositoryColor
                                            : Constant.publicRepositoryColor
    }
}
