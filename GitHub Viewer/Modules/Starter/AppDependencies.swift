import UIKit

class AppDependencies {

    private let repositoryListWireframe = RepositoryListDefaultWireframe()

    func installRootViewControllerIntoWindow(_ window: UIWindow) {
        let rootWireframe = RootWireframe()
        repositoryListWireframe.presentRepositoryList(from: window, with: rootWireframe)
    }
}
