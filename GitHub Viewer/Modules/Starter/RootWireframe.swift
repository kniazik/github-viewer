import UIKit

class RootWireframe {

    func presentRootViewController(_ viewController: UIViewController, in window: UIWindow) {
        let nav = navigationController(from: window)
        nav.viewControllers = [viewController]
    }
}

// MARK: - Private methods
private extension RootWireframe {

    func navigationController(from window: UIWindow) -> UINavigationController {
        return window.rootViewController as! UINavigationController
    }
}
