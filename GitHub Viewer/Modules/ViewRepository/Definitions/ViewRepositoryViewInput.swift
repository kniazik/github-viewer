protocol ViewRepositoryViewInput: class {

    var delegate: ViewRepositoryViewOutput? { get set }

    func updateView()

    func showLoading()

    func hideLoading()

    func showError(message: String)
}
