import Foundation

protocol ViewRepositoryViewOutput {

    func didLoad()

    func pageTitle() -> String?

    func pageRequest() -> URLRequest

    func didTapDoneButton()

    func didStartLoadingRequest(_ request: URLRequest?)

    func didFinishLoadingRequest(_ request: URLRequest?)

    func didFailLoadingRequest(_ request: URLRequest?, withError error: Error)
}
