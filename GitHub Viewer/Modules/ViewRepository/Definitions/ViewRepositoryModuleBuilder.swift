import UIKit

protocol ViewRepositoryModuleBuilder {

    func build(with configuration: ViewRepositoryModuleConfiguration,
               delegate: ViewRepositoryModuleDelegate?) -> UIViewController
}

struct ViewRepositoryModuleConfiguration {

    let title: String?

    let url: URL
}
