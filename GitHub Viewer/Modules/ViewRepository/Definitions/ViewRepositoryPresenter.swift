protocol ViewRepositoryPresenter: ViewRepositoryViewOutput {

    var delegate: ViewRepositoryModuleDelegate? { get }

    func configure(with configuration: ViewRepositoryModuleConfiguration)
}
