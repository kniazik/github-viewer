import Foundation

protocol ViewRepositoryViewModel {

    var title: String? { get }

    var url: URL { get }
}
