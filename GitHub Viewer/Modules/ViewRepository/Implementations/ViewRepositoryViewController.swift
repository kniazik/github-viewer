import UIKit

class ViewRepositoryViewController: UIViewController {

    @IBOutlet fileprivate weak var wvRepository: UIWebView!

    @IBOutlet fileprivate weak var aiLoader: UIActivityIndicatorView!

    var delegate: ViewRepositoryViewOutput?

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate?.didLoad()
    }
}

// MARK: - Actions
private extension ViewRepositoryViewController {

    @IBAction func doneAction(_ sender: UIBarButtonItem) {
        delegate?.didTapDoneButton()
    }
}

// MARK: - ViewRepositoryViewInput
extension ViewRepositoryViewController: ViewRepositoryViewInput {

    func updateView() {
        title = delegate?.pageTitle()

        guard let request = delegate?.pageRequest() else { return }
        wvRepository.loadRequest(request)
    }

    func showLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.aiLoader.isHidden = false
        }
    }

    func hideLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.aiLoader.isHidden = true
        }
    }

    func showError(message: String) {
        let alertController = UIAlertController(title: "Error",
                                                message: message,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(okAction)

        present(alertController, animated: true)
    }
}

// MARK: - UIWebViewDelegate
extension ViewRepositoryViewController: UIWebViewDelegate {

    func webViewDidStartLoad(_ webView: UIWebView) {
        delegate?.didStartLoadingRequest(webView.request)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        delegate?.didFinishLoadingRequest(webView.request)
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        delegate?.didFailLoadingRequest(webView.request, withError: error)
    }
}
