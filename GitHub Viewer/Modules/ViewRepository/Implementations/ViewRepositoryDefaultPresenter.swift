import Foundation

class ViewRepositoryDefaultPresenter: ViewRepositoryPresenter {

    fileprivate(set) weak var delegate: ViewRepositoryModuleDelegate?

    fileprivate weak var view: ViewRepositoryViewInput?

    fileprivate let wireframe: ViewRepositoryWireframeInput

    fileprivate var viewModel: ViewRepositoryViewModel!

    init(view: ViewRepositoryViewInput,
         wireframe: ViewRepositoryWireframeInput,
         delegate: ViewRepositoryModuleDelegate?) {
        self.view = view
        self.wireframe = wireframe
        self.delegate = delegate
    }

    func configure(with configuration: ViewRepositoryModuleConfiguration) {
        viewModel = ViewRepositoryDefaultViewModel(title: configuration.title,
                                                   url: configuration.url)
    }
}

// MARK: - RepositoryListViewOutput
extension ViewRepositoryDefaultPresenter: ViewRepositoryViewOutput {

    func didLoad() {
        view?.updateView()
    }

    func didTapDoneButton() {
        delegate?.dismissModule()
    }

    func pageTitle() -> String? {
        return viewModel.title
    }

    func pageRequest() -> URLRequest {
        return URLRequest(url: viewModel.url, cachePolicy: .returnCacheDataElseLoad)
    }

    func didStartLoadingRequest(_ request: URLRequest?) {
        view?.showLoading()
    }

    func didFinishLoadingRequest(_ request: URLRequest?) {
        view?.hideLoading()
    }

    func didFailLoadingRequest(_ request: URLRequest?, withError error: Error) {
        guard request?.url == viewModel.url else { return }
        view?.showError(message: error.localizedDescription)
    }
}
