import Foundation

struct ViewRepositoryDefaultViewModel: ViewRepositoryViewModel {

    let title: String?

    let url: URL
}
