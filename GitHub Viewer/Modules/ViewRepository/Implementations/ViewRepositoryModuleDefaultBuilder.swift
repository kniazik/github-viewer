import UIKit

class ViewRepositoryModuleDefaultBuilder: ViewRepositoryModuleBuilder {

    private struct Constant {
        static let navViewIdentifier = "NavigationViewRepositoryViewController"
    }

    private var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }

    func build(with configuration: ViewRepositoryModuleConfiguration,
               delegate: ViewRepositoryModuleDelegate?) -> UIViewController {
        let nav = storyboard.instantiateViewController(withIdentifier: Constant.navViewIdentifier) as! UINavigationController
        let view = nav.topViewController as! ViewRepositoryViewInput
        let wireframe: ViewRepositoryWireframeInput = ViewRepositoryDefaultWireframe()
        let presenter: ViewRepositoryPresenter
        presenter = ViewRepositoryDefaultPresenter(view: view, wireframe: wireframe, delegate: delegate)
        presenter.configure(with: configuration)
        view.delegate = presenter

        return nav
    }
}
