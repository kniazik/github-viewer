import Foundation

/**
 Defines set of handles error.
 */
public enum RequestError: Error {

    /// Given `Request` object is not valid to be sent.
    case invalidRequest
    
    /// Actual response has type different from specified `ResponseKind`. Therefore couldn't be handled by `Response`.
    case invalidResponseKind
    
    /// Remote service correctly processed `Request`, but responded with an error.
    case failedRequest

    /// Response has unacceptable status code.
    case validationError(ErrorResponse)
}
