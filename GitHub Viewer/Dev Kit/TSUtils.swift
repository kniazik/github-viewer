import Foundation

// MARK: - Grouping
public extension Sequence {

    /// Groups any sequence by the key specified in the closure and creates a dictionary.
    /// - Note: `key` must conform to `Hashable` protocol.
    /// - Parameter key: A closure that provides a key property of `Sequence` elements to group by.
    /// - Returns: A dictionary with all elements grouped by specified `key`.
    public func group<KeyType: Hashable>(by key: (Iterator.Element) -> KeyType?) -> [KeyType : [Iterator.Element]] {
        let results: [KeyType : Array<Iterator.Element>] = self.reduce([:]) {
            guard let key = key($1) else {
                return $0
            }
            var dic = $0
            if var array = dic[key] {
                array.append($1)
                dic[key] = array
            } else {
                dic[key] = [$1]
            }
            return dic
        }
        return results
    }

    /// Maps `Sequence` to a `Dictionary` using `key` and `value` closures to build resulting `dictionary`.
    /// - Parameter key: A closure that provides a property of `Sequence`'s element to be used as a dictionary key.
    /// - Parameter value: A closure that provides a property of `Sequence`'s element to be used as a dictionary value.
    /// - Note: `key` must be unique in order to avoid collisions.
    /// - Returns: `Dictionary` containing key-value pairs.
    public func map<KeyType: Hashable, ValueType>(key: (Iterator.Element) throws -> KeyType,
                                                  value: (Iterator.Element) throws -> ValueType) -> [KeyType : ValueType] {
        let results: [KeyType : ValueType] = self.reduce([:]) { k, v in
            guard let key = try? key(v),
                  let value = try? value(v) else {
                return k
            }
            var dic = k
            dic[key] = value
            return dic
        }
        return results
    }

    /// Maps `Sequence` to a `Dictionary` using `key` and `value` closures to build resulting `dictionary`.
    /// - Parameter key: A closure that provides a property of `Sequence`'s element to be used as a dictionary key.
    /// - Parameter value: A closure that provides a property of `Sequence`'s element to be used as a dictionary value.
    /// - Note: `key` must be unique in order to avoid collisions.
    /// - Returns: `Dictionary` containing key-value pairs.
    public func flatMap<KeyType: Hashable, ValueType>(key: (Iterator.Element) throws -> KeyType?,
                                                      value: (Iterator.Element) throws -> ValueType?) -> [KeyType : ValueType] {
        let results: [KeyType : ValueType] = self.reduce([:]) { k, v in
            guard let keyBug = try? key(v), // For some reason Swift `guard` fails to unwrap both optional value and optional `try`
                  let key = keyBug,
                  let valueBug = try? value(v),
                  let value = valueBug else {
                return k
            }
            var dic = k
            dic[key] = value
            return dic
        }
        return results
    }
}

// MARK: - Distinct
public extension Sequence where Iterator.Element: Hashable {

    /// Filters duplicated `Hashable` elements of `Sequence`.
    /// - Returns: An array containing only distinct elements of the `Sequence`.
    public var distinct: [Iterator.Element] {
        return Array(Set(self))
    }
}

public extension Sequence where Iterator.Element: Equatable {

    /// Filters duplicated `Equatable` elements of `Sequence`.
    /// - Returns: An array containing only distinct elements of the `Sequence`.
    public var distinct: [Iterator.Element] {
        return self.reduce([]) { uniqueElements, element in
            uniqueElements.contains(element)
                    ? uniqueElements
                    : uniqueElements + [element]
        }
    }
}

public extension Sequence {

    /// Filters duplicated elements of the `Sequence` using element's property provided in `key`
    /// closure.
    /// - Note: Key property must conform to `Equatable` protocol.
    /// - Parameter key: A closure providing a key property of the element to compare.
    /// - Returns: An array containing only distinct elements of the `Sequence`.
    public func distinct<T: Equatable>(by key: (Self.Iterator.Element) -> T?) -> [Iterator.Element] {
        return self.reduce(([], [])) { (unique: ([T], [Iterator.Element]), element: Iterator.Element) in
            guard let key = key(element) else {
                return unique
            }
            return unique.0.contains(key)
                    ? unique
                    : (unique.0 + [key], unique.1 + [element])
        }.1
    }
}

// MARK: - Dictionary filtering
public extension Dictionary {

    /// Creates a new `Dictionary` by filtering current one using `includeElement` closure.
    /// - Parameter includeElement: A closure used as predicate to filter elements.
    /// - Returns: New dictionary containing only filtered elements.
    public func filtered(_ isIncluded: (Iterator.Element) throws -> Bool) rethrows -> [Key : Value] {
        var dict = [Key : Value]()
        let res: [Iterator.Element] = try self.filter(isIncluded)
        res.forEach {
            dict[$0.0] = $0.1
        }
        return dict
    }

    /// Creates a new `Dictionary` by transforming each existing key-value pair using `transform` closure.
    /// - Parameter transform: Closure which applies transformation to key-value pairs.
    ///                        `transform` closure returns a transformed `(key, value)` tuple.
    ///                        In case of either `key`, `value` or tuple is `nil` transformation fails
    ///                        and current key-value pair will be skipped.
    /// - Returns: A new `dictionary` containing successfully transformed key-value pairs.
    public func flatMap<NewKeyType, NewValueType>(_ transform: (Iterator.Element) throws -> (NewKeyType?, NewValueType?)?)
    rethrows -> [NewKeyType : NewValueType] {
        var dict = [NewKeyType : NewValueType]()
        for pair in self {
            if let transformed = try transform(pair),
               let key = transformed.0,
               let value = transformed.1 {
                dict[key] = value
            }
        }
        return dict
    }
}

// MARK: - Filtering
public extension Sequence {

    /// Returns a pair of arrays containing, in order, the elements of the sequence that satisfy the given predicate.
    /// Splits the elements of the sequence into two arrays with elements that satisfy the given predicate and not.
    /// - Parameter isIncluded: A closure that takes an element of the sequence as its argument
    ///                         and returns a Boolean value indicating whether the element
    ///                         should be included in the first returned array or second
    /// - Returns: A pair of arrays where first array contains elements allowed by `isIncluded` and second array contains those elements which are not allowed.
    func filtered(_ isIncluded: (Element) throws -> Bool) rethrows -> ([Element], [Element]) {
        return try reduce(([], [])) { result, element in
            return try isIncluded(element)
                    ? (result.0 + [element], result.1)
                    : (result.0, result.1 + [element])
        }
    }
}

// MARK: - Array's elements value access.
public extension Array where Element: Equatable {

    /**
    Allows to access element in the array by it's value.

    When using `get` you can get an array value before the update.
    ```
     // Assume array contains Equatable objects with unique ids and `property1` set to "value".

     let obj1 = array[0] // obj1.property1 = "value"
     obj1.property1 = "newValue"
     let oldObj1 = array[obj1] // Here oldObj1 will be found by id, thus will contain oldValue.
     print(oldObj1.property1) // Prints "value"
    ```

    When using `set` you can replace an old element with new one at the same position.

    ```
     array[oldObj1] = oldObj1 // or array[obj1]. Doesn't matter as long as obj1 has the same id.
     print(array[0].property1) // Prints "newValue".
    ```
    - Parameter element: An element to be found.
    - Note: `Element` must conform to `Equatable`.
    - Note: Useful when you need to update value in the array and don't know its index.
    - Returns: An element contained in the array.
    */
    subscript(element: Iterator.Element) -> Iterator.Element? {
        get {
            if let index = self.index(of: element) {
                return self[index]
            } else {
                return nil
            }
        }
        set {
            if let index = self.index(of: element) {
                if let newValue = newValue {
                    self[index] = newValue
                } else {
                    self.remove(at: index)
                }
            }
        }
    }
}

/// Appends all keys and values from `right` dictionary to `left`.
public func +=<K, V>(left: inout [K : V], right: [K : V]) {
    right.forEach { (k, v) in
        left[k] = v
    }
}

/// Creates a new dictionary containing all keys and values from both `left` and `right` dictionaries.
public func +<K, V>(left: [K : V], right: [K : V]) -> [K : V] {
    var dic = right
    dic += left
    return dic
}

// MARK: - Random
/// Adds handy random support.
public extension Range {

    /// Gets random value from the interval.
    public var random: Bound {
        let range = (self.upperBound as! Float) - (self.lowerBound as! Float)
        let randomValue = (Float(arc4random_uniform(UInt32.max)) / Float(UInt32.max)) * range + (
                self.lowerBound as! Float)
        return randomValue as! Bound
    }
}

public extension Collection {

    /// Gets random value from the range.
    public var random: Self.Element {
        if let startIndex = self.startIndex as? Int {
            let start = UInt32(startIndex)
            let end = UInt32(self.endIndex as! Int)
            return self[Int(arc4random_uniform(end - start) + start) as! Self.Index]
        }
        var generator = self.makeIterator()
        var count = arc4random_uniform(UInt32(self.count as! Int))
        while count > 0 {
            let _ = generator.next()
            count = count - 1
        }
        return generator.next()!
    }
}

public extension Collection {

    /// Finds such index N that predicate is true for all elements up to
    /// but not including the index N, and is false for all elements
    /// starting with index N.
    /// Behavior is undefined if there is no such N.
    func orderedIndex(where predicate: (Iterator.Element) -> Bool) -> Index {
        return orderedIndex(where: predicate, in: startIndex..<endIndex)
    }

    func orderedIndex(where predicate: (Iterator.Element) -> Bool, in range: Range<Index>) -> Index {
        guard range.lowerBound != range.upperBound else { return range.lowerBound }
        let distance = self.distance(from: range.lowerBound, to: range.upperBound)
        let mid = index(range.lowerBound, offsetBy: distance / 2)
        let isMatching = predicate(self[mid])
        guard distance > 1 else { return isMatching ? range.upperBound : range.lowerBound }
        let myRange = isMatching
                ? mid..<range.upperBound
                : range.lowerBound..<mid
        return orderedIndex(where: predicate, in: myRange)
    }
}

public extension BidirectionalCollection {

    /// Gets the last element matching specified `predicate`. Works the same way as `first
    /// (where:)`, but reversed.
    /// - Returns: Last element matching `predicate`.
    func last(where predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Self.Iterator.Element? {
        for index in self.indices.reversed() {
            let element = self[index]
            if try predicate(element) {
                return element
            }
        }
        return nil
    }
}

public extension Array {

    /// Creates a new array by randomly shuffling elements of current one.
    /// - Returns: An array containing all original elements in shuffled order.
    public var shuffled: Array {
        var shuffled = self
        self.indices.dropLast().forEach { a in
            guard case let b = Int(arc4random_uniform(UInt32(self.count - a))) + a, b != a else {
                return
            }
            shuffled[a] = self[b]
        }
        return self
    }

    /// Creates a new array containing `n` elements from the original array picked up randomly.
    /// - Parameter n: Number of elements to be returned.
    /// - Returns: An array containing `n` elements from current one.
    public func random(_ n: Int) -> Array {
        return Array(self.shuffled.prefix(n))
    }

    /// Moves element at given `index` to new position at `newIndex`.
    /// - Parameter index: An index of the element to be moved.
    /// - Parameter newIndex: An index of the position in the array where element should be placed.
    mutating func move(from index: Int, to newIndex: Int) {
        guard index != newIndex else { return }
        let item = remove(at: index)
        insert(item, at: newIndex > count ? count : newIndex)
    }

    /// Creates new array with an element at given `index` moved to new position at `newIndex`.
    /// - Parameter index: An index of the element to be moved.
    /// - Parameter newIndex: An index of the position in the array where element should be placed.
    /// - Returns: An array with the element moved to new position.
    func moving(from index: Int, to newIndex: Int) -> Array<Element> {
        var copy = self
        copy.move(from: index, to: newIndex)
        return copy
    }
}

extension Array {

    var leftHalf: [Element] {
        let midpoint = self.count / 2
        let half = self[..<midpoint]
        return Array(half)
    }

    var rightHalf: [Element] {
        let midpoint = self.count / 2
        let half = self[midpoint...]
        return Array(half)
    }

    func splited() -> (left: [Element], right: [Element]) {
        return (left: leftHalf, right: rightHalf)
    }
}

import UIKit

extension String {

    /// A single matching group containing matched string and all groups captured from that match.
    typealias MatchingGroup = (match: String, groups: [(match: String, range: Range<String.Index>)])

    /// Extracts groups of substrings matching specified regular expression string `regex`.
    /// Each group contains matched string and an array of captured `regex` groups within that matched string.
    /// - Returns: Groups of substrings matched regex pattern.
    func groups(matchingRegex regex: String) throws -> [MatchingGroup] {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = self as NSString
        let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
        return results.map { result in
            let match = nsString.substring(with: result.range(at: 0))
            let groups: [(String, Range<String.Index>)] = (1..<result.numberOfRanges).flatMap {
                let nsrange = result.range(at: $0)
                guard nsrange.location != NSNotFound,
                      let range = Range(nsrange, in: self) else {
                    return nil
                }
                return (self.substring(with: range), range)

            }
            return (match: match, groups: groups)
        }
    }

    /// Removes `HTML` tags from string.
    /// - Returns: String without `HTML` tags.
    func removingTags() -> String {
        return replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }

    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(startIndex, offsetBy: value.upperBound)]
        }
    }

    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(startIndex, offsetBy: value.upperBound)]
        }
    }

    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(startIndex, offsetBy: value.lowerBound)...]
        }
    }
}

infix operator =~

/** Validates string with given regex pattern
 - Note: Requires exact match of the whole string. No submatches are allowed.
 Thus:
 ```
 "example" =~ ".{3}" // false
 "example" =~ ".{7}" // true
 ```
 - Parameter input: String to be validated.
 - Parameter pattern: Regex pattern to be matched.
 - Returns: Bool value indicating whether input matches the pattern.
 */
public func =~(input: String, pattern: String) -> Bool {
    return input.range(of: pattern, options: .regularExpression) == input.startIndex..<input.endIndex
}
