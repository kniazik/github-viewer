open class ResponseConverter <T> where T : Any {
    open func convert(_ dictionary : [String : Any]) -> T? {return nil}
    
    open func log(_ entity : Any) {
        print("\(type(of: self)): Failed to parse: \n \(entity)")
    }
    
    public init() {}
}

open class RequestConverter <T> where T : Any {
    open func convert(_ model : T) -> [String : Any]? { return nil }
    public init() {}
}
