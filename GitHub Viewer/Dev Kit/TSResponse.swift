/**
 Represents common interface of the TSResponse. Used intensively to handle array of different `RequestCall`'s.
 */
public protocol AnyResponse {

    /// Defines kind of data which response can handle
    /// - Note: Optional. (default: .json)
    static var kind: ResponseKind { get }

    /// Mandatory initializer to set a request related to this response.
    /// - Note: Initializer can fail if it could not handle response body.
    init?(request: Request, status: Int, body: Any)

    /// Request related to the response.
    var request: Request { get }

    /// Status code returned by server.
    var status: Int { get }
}

/**
 Defines a response object.
 */
public protocol TSResponse: AnyResponse {

    /// Type of the handled object.
    associatedtype ObjectType

    /// Contains response object.
    var value: ObjectType { get }
}

/**
 Defines what kind of data `TSResponse` can handle
 */
public enum ResponseKind {

    /// TSResponse handles JSON.
    case json

    /// TSResponse handles NSData.
    case data

    /// TSResponse handles String.
    case string

    /// TSResponse handles no data.
    case empty
}

// MARK: - TSResponse Defaults
public extension TSResponse {

    public static var kind: ResponseKind {
        return .json
    }

    public var description: String {
        if let descr = self.value as? CustomStringConvertible {
            return "Value: \(descr)"
        } else {
            return "Value: \(String(describing: self.value))"
        }
    }
}
