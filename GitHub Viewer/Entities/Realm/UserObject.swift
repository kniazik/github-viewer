import RealmSwift

/// Represents user.
/// - Note: Can be accessed in `Realm` by **id** primary key.
final class UserObject: Object {

    /// Identifier of user.
    @objc dynamic var id: Int64 = 0

    /// User login.
    @objc dynamic var username: String = ""

    /// Primary key for `User` object in `Realm` Database.
    override class func primaryKey() -> String? {
        return "id"
    }

    override var description: String {
        return "[id = \(id), username = \(username)]"
    }
}
