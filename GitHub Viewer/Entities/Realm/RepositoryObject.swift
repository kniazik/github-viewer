import RealmSwift

/// Represents repository.
/// - Note: Can be accessed in `Realm` by **id** primary key.
class RepositoryObject: Object {

    /// Identifier of repository.
    @objc dynamic var id: Int64 = 0

    /// Repository name.
    @objc dynamic var name: String = ""

    /// Repository description.
    @objc dynamic var specification: String = ""

    /// Defines whether repository is `public` or `private`.
    @objc dynamic var isPrivate: Bool = false

    /// Link in `String` format for accessing repository.
    @objc dynamic var url: String = ""

    /// Owner of the repository.
    @objc dynamic var owner: UserObject?

    /// Stars count of repository.
    @objc dynamic var stars: Int = 0

    /// Primary key for `Repository` object in `Realm` Database.
    override class func primaryKey() -> String? {
        return "id"
    }

    override var description: String {
        return "[id: \(id), name: \(name), stars: \(stars)]"
    }
}
