import Foundation

struct Repository: Decodable, CustomStringConvertible {

    let id: Int64

    let name: String

    let specification: String

    let isPrivate: Bool

    let url: URL

    let owner: User

    let stars: Int

    var description: String {
        return "[id: \(id), name: \(name), stars: \(stars)]"
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case specification = "description"
        case isPrivate = "private"
        case url = "html_url"
        case owner
        case stars = "stargazers_count"
    }
}
