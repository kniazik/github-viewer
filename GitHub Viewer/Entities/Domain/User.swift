struct User: Decodable {

    let id: Int64

    let username: String

    enum CodingKeys: String, CodingKey {
        case id
        case username = "login"
    }
}
