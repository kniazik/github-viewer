import RealmSwift

class RealmRepositoryRepository: RepositoryRepository {

    private let realm: Realm

    private let realmRepositorySerializer: RealmRepositorySerializer & RealmRepositoryDeserializer

    init() {
        let configuration = RealmDatabaseConfiguration()
        realm = try! Realm(configuration: configuration.configuration)
        realmRepositorySerializer = RealmRepositoryDefaultSerializer(realmUserSerializer: RealmUserDefaultSerializer())
    }

    var repositories: [Repository] {
        realm.refresh()
        let objects = realm.objects(RepositoryObject.self)
        return objects.map(realmRepositorySerializer.deserialize)
    }

    func insert(_ repositories: [Repository], orUpdateExisting shouldUpdateExisting: Bool) {
        let objects = repositories.map(realmRepositorySerializer.serialize)

        try! realm.write {
            realm.add(objects, update: shouldUpdateExisting)
        }
    }

    func delete(_ repositories: [Repository]) {
        let objects = repositories.map(realmRepositorySerializer.serialize)

        try! realm.write {
            realm.delete(objects)
        }
    }
}
