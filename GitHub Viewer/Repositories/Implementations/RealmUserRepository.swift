import RealmSwift

class RealmUserRepository: UserRepository {

    private let realm: Realm

    private let realmUserSerializer: RealmUserSerializer & RealmUserDeserializer

    init(realmUserSerializer: RealmUserSerializer & RealmUserDeserializer) {
        let configuration = RealmDatabaseConfiguration()
        realm = try! Realm(configuration: configuration.configuration)
        self.realmUserSerializer = realmUserSerializer
    }

    var users: [User] {
        realm.refresh()
        let objects = realm.objects(UserObject.self)
        return objects.map(realmUserSerializer.deserialize)
    }

    func insert(_ users: [User], orUpdateExisting shouldUpdateExisting: Bool) {
        let objects = users.map(realmUserSerializer.serialize)

        try! realm.write {
            realm.add(objects, update: shouldUpdateExisting)
        }
    }

    func delete(_ users: [User]) {
        let objects = users.map(realmUserSerializer.serialize)

        try! realm.write {
            realm.delete(objects)
        }
    }
}
