class RealmUserDefaultSerializer: RealmUserSerializer, RealmUserDeserializer {

    func serialize(_ user: User) -> UserObject {
        let object = UserObject()
        object.id = user.id
        object.username = user.username

        return object
    }

    func deserialize(_ user: UserObject) -> User {
        return User(id: user.id, username: user.username)
    }
}
