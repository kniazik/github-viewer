import Foundation

class RealmRepositoryDefaultSerializer: RealmRepositorySerializer, RealmRepositoryDeserializer {

    private let realmUserSerializer: RealmUserSerializer & RealmUserDeserializer

    init(realmUserSerializer: RealmUserSerializer & RealmUserDeserializer) {
        self.realmUserSerializer = realmUserSerializer
    }

    func serialize(_ repository: Repository) -> RepositoryObject {
        let object = RepositoryObject()
        object.id = repository.id
        object.name = repository.name
        object.specification = repository.specification
        object.isPrivate = repository.isPrivate
        object.url = repository.url.absoluteString
        object.owner = realmUserSerializer.serialize(repository.owner)
        object.stars = repository.stars

        return object
    }

    func deserialize(_ repository: RepositoryObject) -> Repository {
        return Repository(id: repository.id,
                          name: repository.name,
                          specification: repository.specification,
                          isPrivate: repository.isPrivate,
                          url: URL(string: repository.url)!,
                          owner: realmUserSerializer.deserialize(repository.owner!),
                          stars: repository.stars)
    }
}
