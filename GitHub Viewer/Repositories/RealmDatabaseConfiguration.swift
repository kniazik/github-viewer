import RealmSwift
import KeychainSwift

class RealmDatabaseConfiguration {

    var configuration: Realm.Configuration {
        return Realm.Configuration(fileURL: fileUrl,
                                   encryptionKey: encryptionKey)
    }

    private var fileUrl: URL {
        let userDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        return userDir.appendingPathComponent(fileName)
    }

    private var encryptionKey: Data {
        let keychain = KeychainSwift()

        if !UserDefaults.standard.bool(forKey: key) {
            var keyData = Data(count: 64)
            _ = keyData.withUnsafeMutableBytes { mutableBytes in
                SecRandomCopyBytes(kSecRandomDefault, keyData.count, mutableBytes)
            }

            keychain.set(keyData, forKey: key)
            UserDefaults.standard.set(true, forKey: key)
        }

        guard let key = keychain.getData(key) else {
            fatalError("Failed to fetch encryption key from Keychain.")
        }

        return key
    }

    private let key: String

    private let fileName: String

    init() {
        key = "Genesis.Interview"
        fileName = "SearchResult.realm"
    }
}
