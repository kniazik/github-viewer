import Foundation

/// Defines `Repository` repository with read operations.
protocol ReadableRepositoryRepository {

    /// Returns all repositories.
    var repositories: [Repository] { get }
}

/// Defines `Repository` repository with write operations.
protocol WritableRepositoryRepository {

    /// Inserts specified `Repository`s into repository or updates existing if such option specified.
    /// - Parameter repositories: `Repository`s to be inserted.
    /// - Parameter shouldUpdateExisting: Flag indicating whether existing `Repository`s should be updated.
    func insert(_ repositories: [Repository], orUpdateExisting shouldUpdateExisting: Bool)

    /// Deletes specified `Repository`s from repository.
    /// - Parameter repositories: `Repository`s to be deleted.
    func delete(_ repositories: [Repository])
}

protocol RepositoryRepository: ReadableRepositoryRepository, WritableRepositoryRepository {}
