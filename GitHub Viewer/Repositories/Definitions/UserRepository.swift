import Foundation

/// Defines `User` repository with read operations.
protocol ReadableUserRepository {

    var users: [User] { get }
}

/// Defines `User` repository with write operations.
protocol WritableUserRepository {

    /// Inserts specified `User`s into repository or updates existing if such option specified.
    /// - Parameter users: `User`s to be inserted.
    /// - Parameter shouldUpdateExisting: Flag indicating whether existing `User`s should be updated.
    func insert(_ users: [User], orUpdateExisting shouldUpdateExisting: Bool)

    /// Deletes specified `User`s from repository.
    /// - Parameter users: `User`s to be deleted.
    func delete(_ users: [User])
}

protocol UserRepository: ReadableUserRepository, WritableUserRepository {}
