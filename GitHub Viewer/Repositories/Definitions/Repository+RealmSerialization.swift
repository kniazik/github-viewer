protocol RealmRepositorySerializer {

    func serialize(_ repository: Repository) -> RepositoryObject
}

protocol RealmRepositoryDeserializer {

    func deserialize(_ repository: RepositoryObject) -> Repository
}
