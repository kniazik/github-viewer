protocol RealmUserSerializer {

    func serialize(_ user: User) -> UserObject
}

protocol RealmUserDeserializer {

    func deserialize(_ user: UserObject) -> User
}
