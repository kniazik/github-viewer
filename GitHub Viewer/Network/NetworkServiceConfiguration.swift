struct NetworkServiceConfiguration: RequestManagerConfiguration {

    let baseUrl = "https://api.github.com"

    let headers = ["Content-Type" : "application/json"]

    let timeout = 90
}
