struct SearchRequest: Request {

    let method: RequestMethod = .get

    let url: String = "search/repositories"

    let parameters: [String : Any]?

    init(for keyword: String,
         sortedBy sorting: Sorting = .stars,
         orderedBy ordering: Ordering = .descending) {
        parameters = [Keys.keyword : keyword,
                      Keys.sorting : sorting.rawValue,
                      Keys.ordering : ordering.rawValue]
    }
}

extension SearchRequest {

    enum Sorting: String {
        case stars
        case forks
        case updated
    }

    enum Ordering: String {
        case ascending = "asc"
        case descending = "desc"
    }

    private struct Keys {
        static let keyword = "q"
        static let sorting = "sort"
        static let ordering = "order"
    }
}
