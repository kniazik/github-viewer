struct SearchResponse: TSResponse {

    static let kind: ResponseKind = .json

    let request: Request

    let value: (keyword: String, repositories: [Repository])

    let status: Int

    init?(request: Request, status: Int, body: Any) {
        guard let body = body as? [String : Any],
              let items = body["items"] as? [[String : Any]] else { return nil }
        guard let keyword = request.parameters?["q"] as? String else { return nil }

        let deserializer: RepositoryDeserializer = RepositoryDefaultSerializer()
        let repositories = items.compactMap(deserializer.deserialize)

        self.request = request
        self.status = status

        value = (keyword: keyword, repositories: repositories)
    }
}
