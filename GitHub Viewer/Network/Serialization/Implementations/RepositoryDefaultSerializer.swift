import Foundation

class RepositoryDefaultSerializer: RepositoryDeserializer {

    private static let decoder = JSONDecoder()

    func deserialize(_ dictionary: [String : Any]) -> Repository? {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: dictionary,
                                                         options: .prettyPrinted) else {
            print("Failed to convert \(dictionary) to JSON.")
            return nil
        }

        return try? RepositoryDefaultSerializer.decoder.decode(Repository.self, from: jsonData)
    }
}
