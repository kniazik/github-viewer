/// Serializer for `Repository` entity.
protocol RepositorySerializer {

    /// Serializes specified `repository` into a `Dictionary`.
    /// - Parameter repository: `Repository` to be serialized.
    /// - Returns: `Dictionary` representation of a `repository`.
    func serialize(_ repository: Repository) -> [String : Any]
}

/// Deserializer for `Repository` entity.
protocol RepositoryDeserializer {

    /// Deserializes specified `dictionary` into a `Repository`.
    /// - Parameter dictionary: `Dictionary` to be deserialized.
    /// - Returns: `Repository` if deserialization is successful, otherwise `nil`.
    func deserialize(_ dictionary: [String : Any]) -> Repository?
}
