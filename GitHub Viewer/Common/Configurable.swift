/// Defines an interface to configure object with acceptable `DataSourceType`s.
protocol Configurable {

    /// Generic type of the data source.
    associatedtype DataSourceType

    /// Configures itself with provided data source.
    /// - Parameter dataSource: Any object conforming to `DataSourceType` protocols acceptable by this `Configurable`.
    func configure(with dataSource : DataSourceType)
}
